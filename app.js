Matrix.Application = function(){
    return {
        /**
         * Addition of two given matrices
         * @param m1 the first matrix object
         * @param m2 the second matrix object
         * @returns {Array} representing the data which can be used to create a new matrix
         */
        add: function(m1, m2){
            var m1Data = m1.getMatrix();
            var m2Data = m2.getMatrix();
            var result = [];
            if(m1Data.length == m2Data.length){
                var height = m1Data.length;
                for(var h = 0; h < height; h++){
                    result.push([]);
                    if(m1Data[h].length == m2Data[h].length){
                        var width = m1Data[h].length;
                        for(var w = 0; w < width; w++){
                            result[h].push([+m1Data[h][w] + +m2Data[h][w]])
                        }
                    }else{
                        throw "Matrices have different Dimensions!";
                    }
                }
            }else{
                throw "Matrices have different Dimensions!";
            }
            return result;
        },

        /**
         * Scales a given matrix with a scalar
         * @param m the matrix object
         * @param scalar the scalar as a number
         * @returns {Array} representing the data which can be used to create a new matrix
         */
        scale: function(m, scalar){
            var result = [];
            var matrix = m.getMatrix();
            if(typeof  scalar === "number") {
                var height = matrix.length;
                for (var h = 0; h < height; h++) {
                    result.push([]);
                    var width = matrix[h].length;
                    for (var w = 0; w < width; w++) {
                        result[h].push([matrix[h][w] * scalar])
                    }
                }
            }else{
                throw "Scalar is not a number!";
            }

            return result;
        },

        /**
         * Multiplies two given matrices
         * @param m1 the first matrix object
         * @param m2 the second matrix object
         * @returns {Array} representing the data which can be used to create a new matrix
         */
        multiply: function(m1, m2){
            var m1Data = m1.getMatrix();
            var m2Data = m2.getMatrix();
            var result = [];
            var m1height = m1Data.length;
            for(var h = 0; h < m1height; h++){
                result.push([]);
                var m2width = m2Data[h].length;
                for(var w = 0; w < m2width; w++){
                    var row = m1.getRow(h);
                    var column = m2.getColumn(w);
                    var sum = 0;

                    if(row.length == column.length){
                        for(var i = 0; i < row.length; i++){
                            sum += row[i]*column[i];
                        }
                    }else{
                        throw "The dimensions of those arrays mismatch, try to interchange them or try different arrays!";
                    }

                    result[h].push(sum);
                }
            }

            return result;
        },

        /**
         * Transposes a given matrix
         * @param matrix the matrix to transpose
         * @returns {Array} representing the data which can be used to create a new matrix
         */
        transpose: function(matrix){
            var result = [];
            var matrixData = matrix.getMatrix();

            if(matrixData[0].length === undefined){
                throw "Something went wrong";
            }else{
                var matrixWidth = matrixData[0].length;
            }

            for(var i = 0; i < matrixWidth; i++){
                result.push(matrix.getColumn(i));
            }
            return result;
        }
    }
}
