var Matrix = function(){
    return {
        matrix: [],

        /**
         * Returns the matrix as Array
         * @returns {Array}
         */
        getMatrix: function(){
            return this.matrix;
        },

        /**
         * Sets the matrix for this object
         * @param matrix A two-dimensional Array
         */
        setMatrix: function(matrix){
            this.matrix = matrix;
        },

        /**
         * Returns a specific row of this objects matrix
         * @param id
         * @returns {Array}
         */
        getRow: function(id){
            return this.matrix[id];
        },

        /**
         * Returns a specific column of this objects matrix
         * @param id
         * @returns {Array}
         */
        getColumn: function(id){
            var tmpMatrix = [];
            for(var i = 0; i < this.matrix.length; i++){
                tmpMatrix.push(this.matrix[i][id]);
            }
            return tmpMatrix;
        }
    }
}
